var edges = {
    "home": [
        "about",
        "products",
        "events",
        "contact"
    ],
    "about": [],
    "products": [
        "shoes"
    ],
    "events": [
        "hackathon"
    ],
    "contact": [],
    "shoes": [
        "running",
        "basketball",
        "fancy",
        "boots",
        "slippers"
    ]
};