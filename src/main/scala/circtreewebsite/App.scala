package circtreewebsite

import circtreewebsite.controller.GraphController
import org.scalajs.dom
import org.scalajs.dom.ext._

object App {
  var graph: GraphController = _

  def main(): Unit = {
    org.scalajs.dom.console.log("JavaScript working")
    dom.window.onload = { _ => setup() }
  }

  private def setup(): Unit = {
    setupGraph()
    org.scalajs.dom.console.log("Setup graph")
  }

  private def setupGraph(): Unit = {
    val containerElem = dom.document.getElementById("nodes")
    graph = GraphController.create(
      nodes = containerElem.children.toTraversable,
      edges = Globals.edges.toMap.mapValues(_.toSeq),
      containerElem = containerElem
    )
  }
}
