package circtreewebsite.controller

import circtreewebsite.graph._
import circtreewebsite.ui.{Circle, CircleEdge}
import circtreewebsite.util.Vec
import org.scalajs.dom
import org.scalajs.dom.raw.HTMLDivElement
import org.querki.jquery._

class GraphController(val graph: Graph[Circle, CircleEdge],
                      val containerElem: dom.Element,
                      initNode: Node[Circle, CircleEdge]) {
  private var _currentNode: Node[Circle, CircleEdge] = initNode

  def currentNode: Node[Circle, CircleEdge] = _currentNode

  private def setup(): Unit = {
    graph.nodes.idxSeq.foreach(setupNode)
  }

  private def deconfigure(): Unit = {
    graph.nodes.idxSeq.foreach(deconfigureNode)
  }

  private def setupNode(node: Node[Circle, CircleEdge]): Unit = {
    val containerSize = Vec(containerElem.clientWidth, containerElem.clientHeight)
    val containerCenter = Vec(containerSize.x / 2, containerSize.y / 2)
    val elem = node.content.elem

    val (nodeClass: String, scale: Double, clickHandler: Option[() => Unit @unchecked]) = {
      if (node.id == currentNode.id) {
        (
          "main",
          Circle.mainScale,
          None
        )
      } else {
        node.inEdges.find { _.id.from == currentNode.id }.map { edge =>
          (
            "child",
            Circle.outerScale,
            Some(() => selectChild(node))
          )
        }.orElse(node.outEdges.find { _.id.to == currentNode.id }.map { edge =>
          (
            "parent",
            Circle.outerScale,
            Some(() => selectParent(node))
          )
        }).getOrElse {
          if (node.id.tag == "home") {
            ("home-ancestor", Circle.homeAncestorScale, Some(() => selectHomeAncestor(node)))
          } else {
            ("offscreen", 0, None)
          }
        }
      }
    }
    elem.classList.add(nodeClass)

    val maxDistance = {
      if (node.id.tag == "home") {
        Int.MaxValue
      } else {
        2
      }
    }
    val centerOffset = graph.pathBetween(currentNode, node, maxDistance) match {
      case Some(path) =>
        if (path.isEmpty) {
          Vec.zero
        } else {
          val pathSizeMult = {
            if (path.edges.length > 1) {
              1.25
            } else {
              1.0
            }
          }

          path.edges.map { biEdge =>
            val edgeDirection = biEdge.direction match {
              case Forward => biEdge.edge.content.outDirection
              case Backward => biEdge.edge.content.inDirection
            }
            Vec.radial(
              CircleEdge.distance(containerSize),
              edgeDirection
            )
          }.reduce(_ + _) / path.edges.size * pathSizeMult
        }
      case None => Vec.zero //Too far away to care
    }
    val center = containerCenter + centerOffset

    def reposition(scale: Double): Unit = {
      val size = containerSize.minDim * scale
      elem.style.width = size + "px"
      elem.style.height = size + "px"
      elem.style.left = (center.x - size / 2) + "px"
      elem.style.top = (center.y - size / 2) + "px"
    }

    if (nodeClass != "main") {
      $(elem).on("mouseenter.setup", { () =>
        reposition(scale * Circle.hoverScaleMult)
      })
      $(elem).on("mouseleave.setup", { () =>
        reposition(scale)
      })
    }
    clickHandler.foreach { clickHandler =>
      $(elem).on("click.setup", { () => clickHandler() })
    }

    reposition(scale)
  }

  private def deconfigureNode(node: Node[Circle, CircleEdge]): Unit = {
    val elem = node.content.elem

    elem.classList.remove("main")
    elem.classList.remove("child")
    elem.classList.remove("parent")
    elem.classList.remove("home-ancestor")
    elem.classList.remove("offscreen")
    $(elem).unbind(".setup")
  }

  private def selectChild(node: Node[Circle, CircleEdge]): Unit = {
    deconfigure()
    _currentNode = node
    setup()
  }

  private def selectParent(node: Node[Circle, CircleEdge]): Unit = {
    deconfigure()
    _currentNode = node
    setup()
  }

  private def selectHomeAncestor(node: Node[Circle, CircleEdge]): Unit = {
    deconfigure()
    _currentNode = node
    setup()
  }

  setup()
}

object GraphController {
  def create(nodes: Traversable[dom.Element], edges: Traversable[(String, Seq[String])], containerElem: dom.Element): GraphController = {
    val graph = Graph(
      nodes.map { nodeElem =>
        val nodeDiv = nodeElem.asInstanceOf[HTMLDivElement]
        nodeDiv.classList.add("node")
        val nodeId = nodeDiv.id
        val nodeCircle = Circle(nodeDiv)
        (nodeId, nodeCircle)
      }.toSeq,
      edges.flatMap {
        case (fromTag, toTags) => toTags.zipWithIndex.map {
          case (toTag, idx) => ((fromTag, toTag), CircleEdge(idx, toTags.size))
        }
      }.toMap
    )
    new GraphController(
      graph,
      containerElem,
      initNode = graph.nodes.tagMap("home"),
    )
  }
}
