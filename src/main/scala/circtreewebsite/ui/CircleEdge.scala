package circtreewebsite.ui

import circtreewebsite.util.Vec

case class CircleEdge(index: Int, groupCount: Int) {
  val inDirection: Double = {
    Math.PI + (((groupCount / 2.0) - index - 0.5) * CircleEdge.groupSepAngle)
  }

  val outDirection: Double = {
    Math.PI - inDirection
  }
}

object CircleEdge {
  private val groupSepAngle: Double = Math.PI / 5

  def distance(containerSize: Vec): Double = {
    containerSize.minDim * 0.4
  }
}