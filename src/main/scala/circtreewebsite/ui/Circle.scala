package circtreewebsite.ui

import circtreewebsite.util.Vec
import org.scalajs.dom.raw.HTMLDivElement

case class Circle(elem: HTMLDivElement) {

}

object Circle {
  val mainScale: Double = 0.5
  val outerScale: Double = 0.15
  val homeAncestorScale: Double = 0.1
  val hoverScaleMult: Double = 1.25

  def homeAncestorPos(containerSize: Vec): Vec = {
    val cornerOffset = Math.round(containerSize.minDim * 0.1).toInt
    Vec(
      x = containerSize.x - cornerOffset,
      y = cornerOffset
    )
  }
}